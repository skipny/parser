﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Parser.Data;

namespace Parser.Controllers
{
    public class ShopProductController : Controller
    {
        private readonly ProductPricesContext _context;

        public ShopProductController(ProductPricesContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Test()
        {
            
            using var httpClient = new HttpClient() { Timeout = TimeSpan.FromSeconds(10) };
            var respounce = await httpClient.GetAsync("https://www.taen.ru/catalog/vanny/komplektuyushie-i-aksessuary-dlya-vanny/santek-panel-frontalnaya-goa-150h100-levaya/");
            var respounseData = await respounce.Content.ReadAsStringAsync();
            if(respounce.StatusCode == HttpStatusCode.OK)
            {
                var startBlock = "<meta itemprop=\"price\" content=\"";
                var startPos = respounseData.IndexOf(startBlock);
                var priceAllText = respounseData.Substring(startPos+ startBlock.Length);

                var priceText = priceAllText.Substring(0, priceAllText.IndexOf('"'));
                var price = Convert.ToInt32(priceText);
            }
            return Ok();
        }

        // GET: ShopProduct
        public async Task<IActionResult> Index()
        {
            var productPricesContext = _context.ProductsProducts.Include(s => s.Product).Include(s => s.Shop);

            using var httpClient = new HttpClient() { Timeout = TimeSpan.FromSeconds(10) };

            var ll = await productPricesContext.ToListAsync();
            for (var i = 0; i < ll.Count; i++)
            {
                var url = ll[i].Link;
                var respounce = await httpClient.GetAsync(url);
                var respounseData = await respounce.Content.ReadAsStringAsync();
                if (respounce.StatusCode == HttpStatusCode.OK)
                {
                    //var startBlock = "<meta itemprop=\"price\" content=\"";
                    var startBlock = "data-price=\"";
                    var startPos = respounseData.IndexOf(startBlock);
                    var priceAllText = respounseData.Substring(startPos + startBlock.Length);

                    var priceText = priceAllText.Substring(0, priceAllText.IndexOf('"'));
                    var price = Convert.ToDouble(priceText);
                    ll[i].Price = price;
                }
                _context.ProductsProducts.Update(ll[i]);
                _context.SaveChangesAsync();
            }

            return View(ll);

        }

        // GET: ShopProduct/Create
        public IActionResult Create()
        {
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name");
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name");
            return View();
        }

        // POST: ShopProduct/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ProductId,ShopId,Link,Price")] ShopProduct shopProduct)
        {
            if (ModelState.IsValid)
            {
                _context.Add(shopProduct);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ProductId"] = new SelectList(_context.Products, "ProductId", "Name", shopProduct.ProductId);
            ViewData["ShopId"] = new SelectList(_context.Shops, "ShopId", "Name", shopProduct.ShopId);
            return View(shopProduct);
        }

        public async Task<IActionResult> Remove(int? ProductId)
        {
            var variant = _context.ProductsProducts.FirstOrDefault(H => H.ProductId == ProductId);

            if (variant != null)
            {
                _context.ProductsProducts.Remove(variant);
                _context.SaveChanges();

            }
            return RedirectToAction(nameof(Index)); ;
        }
        private bool ShopProductExists(int id)
        {
          return _context.ProductsProducts.Any(e => e.ProductId == id);
        }
    }
}
