﻿using System.ComponentModel.DataAnnotations;

namespace Parser.Data
{
    public class Shop
    {
        [Key]
        public int ShopId { get; set; }
        public string Name { get; set; }
    }
}
