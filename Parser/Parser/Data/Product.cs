﻿using System.ComponentModel.DataAnnotations;

namespace Parser.Data
{
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        public string Name { get; set; }
    }
}
